
#include <RooAbsReal.h>
#include <RooPoisson.h>
#include "Utils.cxx"
#include "RooStats/HistFactory/LinInterpVar.h"


using namespace RooFit;

  
void FullLikelihood() {

  // Define the regularisation parameter.
  Double_t tau = 0.01;

  // Define the mean and sigma of the Gaussian pdf of the nuisance parameters.
  Double_t m_1 = 0;
  Double_t m_2 = 0;
  std::vector<Double_t> sigma_m;
  sigma_m.push_back(1);
  sigma_m.push_back(1);
  
  std::string inputFile("input_histograms.root");

  TFile input(inputFile.c_str());

  TH1D* truthHist = (TH1D*)input.Get("truth_nom");
  TH1D* recoHist = (TH1D*)input.Get("reco_nom");
  TH1D* dataHist = (TH1D*)input.Get("data");
  TH1D* truthTestHist = (TH1D*)input.Get("truth_test");
  TH2D* respHist = (TH2D*)input.Get("response_nom");
  
  Int_t n_reco_bins = dataHist->GetNbinsX();
  Int_t n_truth_bins = truthHist->GetNbinsX();
  
  RooArgList dataBinVars;
  RooArgList recoBinVars;
  RooArgList truthBinVars;
  RooArgList pdfList;

  Int_t nToys = 1000;

  gRandom->SetSeed(0);
  
  // Create a list with variables for all the truth bins.
  for (int i = 0; i < n_truth_bins; i++){

    std::string truthBinName("mu_");
    truthBinName.append(std::to_string(i));
    
    RooRealVar* truthBinVar = new RooRealVar(truthBinName.c_str(),truthBinName.c_str(),truthHist->GetBinContent(i+1),0,100000);
    truthBinVar->setError(0.1);
    
    truthBinVars.add(*truthBinVar);
  }
  
  // Each RooFormulaVar defines each reco bin in terms
  // of response matrix elements and truth bins i.e.
  // nu_{i} = Sum_{j=1} R_{ij}mu{j}
  std::vector<RooFormulaVar> nuFormulas;

  // A list with the nuisance parameter variables.
  RooArgList NPVars;

  // Create the variables for the nuisance parameters.
  // RooRealVar* NP1 = new RooRealVar("theta1","theta1",0);
  // RooRealVar* NP2 = new RooRealVar("theta2","theta2",0);
  RooRealVar* NP1 = new RooRealVar("theta1","theta1",0,-50,50);
  RooRealVar* NP2 = new RooRealVar("theta2","theta2",0,-50,50);
  NP1->setError(0.0001);
  NP2->setError(0.0001);
  NPVars.add(*NP1);
  NPVars.add(*NP2);

  // Create a dataset that will contain the post-fit values
  // of the nuisance parameters.
  RooDataSet NPVals("NPVals","NPVals",NPVars);

  TH2D* resp_NP_1_up = (TH2D*)input.Get("response_1_up");
  TH2D* resp_NP_1_down = (TH2D*)input.Get("response_1_down");
  
  TH2D* resp_NP_2_up = (TH2D*)input.Get("response_2_up");
  TH2D* resp_NP_2_down = (TH2D*)input.Get("response_2_down");

  // Pick the normalization for the response matrices.
  TH1D* resp_NP_1_up_norm = (TH1D*)input.Get("truth_1_up");
  TH1D* resp_NP_1_down_norm = (TH1D*)input.Get("truth_1_down");
  TH1D* resp_NP_2_up_norm = (TH1D*)input.Get("truth_2_up");
  TH1D* resp_NP_2_down_norm = (TH1D*)input.Get("truth_2_down");

  std::vector<TH2D*>up_hists;
  std::vector<TH2D*>down_hists;
  std::vector<TH1D*>up_norm_hists;
  std::vector<TH1D*>down_norm_hists;

  up_hists.push_back(resp_NP_1_up);
  up_hists.push_back(resp_NP_2_up);
  down_hists.push_back(resp_NP_1_down);
  down_hists.push_back(resp_NP_2_down);

  up_norm_hists.push_back(resp_NP_1_up_norm);
  up_norm_hists.push_back(resp_NP_2_up_norm);
  down_norm_hists.push_back(resp_NP_1_down_norm);
  down_norm_hists.push_back(resp_NP_2_down_norm);

  // Create a matrix of linear interpolation variables i.e. one for each response
  // matrix element.
  std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>> responseFunctions;
  getResponseFunctions(NPVars, respHist, truthHist, up_hists, down_hists, up_norm_hists, down_norm_hists, responseFunctions);
    
  // Create the formulas that forward fold the truth bins with the
  // linear interpolated response matrix elements.
  makeNuFormulaWithNPs(truthBinVars, nuFormulas, responseFunctions);

  // Define the NLL constraint term formula that constrains on smoothness:
  // S(mu) = - tau * Sum_{i}^{M-2}(-mu_{i} + 2mu_{i+1} - mu_{i+2})^{2}
  RooFormulaVar Smu = makeTikhonovFormula(truthBinVars,tau);

  // Define the NLL constraint term formula that constrains on the difference between
  // the truth parameters and a truth benchmark:
  // S(mu) = tau * Sum_{i}^{M}(mu_{i} - muBench_{i})^{2}
  //RooFormulaVar Smu = makeRefFormula(truthBinVars,truthTestHist,tau);

  // Loop over all the truth bins and construct a Poisson pdf for each.
  for (int i = 0; i < n_reco_bins; i++){

    std::string pdfName("poisPdf_bin_");
    std::string dataBinName("n_");
    
    pdfName.append(std::to_string(i));
    dataBinName.append(std::to_string(i));

    // Create a variable for the data bin count.
    RooRealVar* dataBinVar = new RooRealVar(dataBinName.c_str(),dataBinName.c_str(),dataHist->GetBinContent(i+1));

    // Create a Poisson p.d.f. for the likelihood.
    RooPoisson* poisPdf = new RooPoisson(pdfName.c_str(),pdfName.c_str(),*dataBinVar,nuFormulas.at(i));

    dataBinVars.add(*dataBinVar);

    pdfList.add(*poisPdf);
  }

  // These values represent an auxiliary measurement or theoretical guess
  // with an uncertainty on it for each nuisance parameter.
  RooRealVar* NP1_mean = new RooRealVar("theta1_mean","theta1_mean",0);
  RooRealVar* NP2_mean = new RooRealVar("theta2_mean","theta2_mean",0);
  // RooRealVar* NP1_mean = new RooRealVar("theta1_mean","theta1_mean",0,-50,50);
  // RooRealVar* NP2_mean = new RooRealVar("theta2_mean","theta2_mean",0,-50,50);
  RooRealVar* NP1_sig = new RooRealVar("theta1_sig","theta1_sig",sigma_m.at(0));
  RooRealVar* NP2_sig = new RooRealVar("theta2_sig","theta2_sig",sigma_m.at(1));

  RooGaussian* theta1Constr = new RooGaussian("theta1pdf","theta1pdf",*NP1_mean,*((RooRealVar*)NPVars.at(0)),*NP1_sig);
  RooGaussian* theta2Constr = new RooGaussian("theta2pdf","theta2pdf",*NP2_mean,*((RooRealVar*)NPVars.at(1)),*NP2_sig);
  
  // Add the Gaussian constraint terms to the pdf list.
  pdfList.add(*theta1Constr);
  pdfList.add(*theta2Constr);
  
  // Set the value for the auxiliary measurement.
  NP1_mean->setVal(m_1);
  NP2_mean->setVal(m_2);
  
  // Add the auxiliary measurements to the data variables.
  dataBinVars.add(*NP1_mean);
  dataBinVars.add(*NP2_mean);

  // Take the product of all pdfs.
  RooProdPdf LH("Likelihood","Likelihood",pdfList);  

  // Make a dataset for all the data bin variables.
  RooDataSet in("indata","indata",dataBinVars);  

  // Add this one datapoint. Only the auxiliary measurement changes to
  // isolate the effect of the systematics.
  in.add(dataBinVars);
  
  // Create the negative log-likelihood.
  RooAbsReal* nll = LH.createNLL(in);
  
  // Add the constraint term to the NLL.
  RooFormulaVar nllConstr("constrNLL","@0 + @1",RooArgList(*nll,Smu));  
  
  TVectorD unfolded_vec(truthHist->GetNbinsX());
  
  // Create a minuit instance.
  RooMinuit *roomin_wReg = new RooMinuit(nllConstr);

  // Create a minuit instance without the constraint.
  RooMinuit *roomin = new RooMinuit(*nll);
  
  // Define the computational rigorousness:
  // (1=fast,2=slow but more accurate,3=intermediate)
  roomin_wReg->setStrategy(2);
  
  // Set the print level output.
  roomin_wReg->setPrintLevel(0);
  
  // Set precision.
  //roomin->setEps(0.000001);

  // Minimize.
  roomin_wReg->migrad();

  // 
  TH1D* hessewRegErrors = (TH1D*)truthTestHist->Clone();
  TH1D* hesseErrors = (TH1D*)truthTestHist->Clone();
  TH1D* unfolded = (TH1D*)truthTestHist->Clone();

  // Get the hessian of the regularized likelihood.
  roomin_wReg->hesse();
  for (int i = 0; i < unfolded->GetNbinsX(); i++){

    unfolded->SetBinContent(i+1,((RooRealVar*)truthBinVars.at(i))->getVal());
    hessewRegErrors->SetBinContent(i+1, ((RooRealVar*)truthBinVars.at(i))->getError());
  }

  // Get the hessian for the unregularized likelihood but
  // at the same minimum of the regularized likelihood.
  roomin->hesse();

  for (int i = 0; i < unfolded->GetNbinsX(); i++){
    hesseErrors->SetBinContent(i+1, ((RooRealVar*)truthBinVars.at(i))->getError());
  }

  delete nll;
  delete roomin;

  TFile output("fulllikelihood.root","RECREATE");

  // Save the results.
  unfolded->Write("unfolded");
  truthTestHist->Write("truth");
  hessewRegErrors->Write("errors_wReg");
  hesseErrors->Write("errors");

  output.Close();

  input.Close();






}
