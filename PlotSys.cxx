


void PlotSys() {

  // Get the input file create with Generate.cxx
  std::string inputFile("input_bimodal.root");
  
  TFile input(inputFile.c_str());

  // Choose the distributions to plot.
  TH1D* nom = (TH1D*)input.Get("reconom");
  TH1D* up = (TH1D*)input.Get("recoNP1up");
  TH1D* down = (TH1D*)input.Get("recoNP1down");
  
  // Create the ratio plots.
  TH1D* ratio_up = (TH1D*)up->Clone();
  TH1D* ratio_down = (TH1D*)down->Clone();
  ratio_up->Divide(nom);
  ratio_down->Divide(nom);

 // Correct for variable bin width.
  for (int i = 0; i < nom->GetNbinsX(); i++){
    Double_t dx = nom->GetBinLowEdge(i+2) - nom->GetBinLowEdge(i+1);

    nom->SetBinContent(i+1, nom->GetBinContent(i+1)/dx);
    up->SetBinContent(i+1, up->GetBinContent(i+1)/dx);
    down->SetBinContent(i+1, down->GetBinContent(i+1)/dx);
  }

  TCanvas c("c","c",600,400);

  TPad p_1("p_1","p_1",0,0.3,1,1);
  TPad p_2("p_2","p_2",0,0,1,0.3);

  c.cd();
  p_1.SetBottomMargin(0.02);
  p_1.SetLeftMargin(0.2);
  p_1.Draw();
  p_1.cd();

  gStyle->SetOptStat(0);

  gPad->SetLeftMargin(0.2);
  gPad->SetLogy(false);

  nom->SetLineColor(kBlack);
  nom->GetYaxis()->SetRangeUser(0.01,1.3*nom->GetMaximum());
  nom->GetYaxis()->SetTitle("Events");
  nom->GetXaxis()->SetTitle("x");
  nom->SetTitle("");
  nom->Draw("HIST");

  up->SetLineColor(38);
  up->SetTitle("");
  up->Draw("HISTSAME");

  down->SetLineColor(46);
  down->SetTitle("");
  down->Draw("SAMEHIST");  

  TLegend leg(0.75, 0.75, 0.9, 0.9);
  leg.SetLineWidth(0);
  leg.SetFillStyle(0);
  leg.AddEntry( nom, "Nom", "l" );
  leg.AddEntry( up, "Up", "l" );
  leg.AddEntry( down, "Down", "l" );
  leg.Draw("SAME");

  c.cd();
  p_2.SetTopMargin(0.01);
  p_2.SetBottomMargin(0.5);
  p_2.SetLeftMargin(0.2);
  p_2.Draw();
  p_2.cd();
  p_2.SetGrid();
  gPad->SetTickx(1);
  gPad->SetTicky(1);

  ratio_up->SetTitle("");
  ratio_up->GetYaxis()->SetTitle("Var./Nom.");
  ratio_up->GetXaxis()->SetLabelSize(0.06);
  ratio_up->GetXaxis()->SetTitle("x");
  ratio_up->GetXaxis()->SetTitleSize(0.1);
  ratio_up->GetYaxis()->SetTitleSize(0.08);
  ratio_up->GetYaxis()->SetTitleOffset(0.5);
  ratio_up->GetYaxis()->SetLabelSize(0.07);
  ratio_up->SetLineColor(38);
  ratio_up->SetLineWidth(2);
  ratio_up->GetYaxis()->SetRangeUser(0.7,1.5);
  ratio_down->SetLineWidth(2);
  ratio_down->SetLineColor(46);

  ratio_up->Draw("HIST");  
  ratio_down->Draw("HIST SAME");

  c.SaveAs("systemplates.pdf");
  
}
