

void ResponseTemplates() {

  //std::string inputFile("input_bimodal.root");
  std::string inputFile("input_exp.root");
  
  TFile input(inputFile.c_str());

  TH1D* truthHist = (TH1D*)input.Get("truthnom");
  TH1D* recoHist = (TH1D*)input.Get("reconom");
  TH1D* recoBkgHist = (TH1D*)input.Get("reco_bkgnom");

  TH1D* dataHist = (TH1D*)input.Get("dataHist");
  TH1D* truthTestHist = (TH1D*)input.Get("truthTestHist");
  TH2D* respHist = (TH2D*)input.Get("responsenom");

  TH2D* response_1_up = (TH2D*)input.Get("responseNP1up");
  TH2D* response_1_down = (TH2D*)input.Get("responseNP1down");

  TH2D* response_2_up = (TH2D*)input.Get("responseNP2up");
  TH2D* response_2_down = (TH2D*)input.Get("responseNP2down");

  TH2D* response_3_up = (TH2D*)input.Get("responseNP3up");
  TH2D* response_3_down = (TH2D*)input.Get("responseNP3down");

  std::vector<TH2D*> templates;
  std::vector<TVectorD> results;
  
  templates.push_back(respHist);
  templates.push_back(response_1_up);
  templates.push_back(response_1_down);
  templates.push_back(response_2_up);
  templates.push_back(response_2_down);
  templates.push_back(response_3_up);
  templates.push_back(response_3_down);

  Int_t n_reco_bins = dataHist->GetNbinsX();
  Int_t n_truth_bins = truthHist->GetNbinsX();

  for (int i = 0; i < templates.size(); i++){

    // Make a spectator object.
    RooUnfoldSpec spec("unfold","unfold",truthHist,"obs_truth",recoHist,"obs_reco",templates.at(i),recoBkgHist,dataHist,false,0.0005);

    // Define the unfolding algorithm.
    RooUnfolding::Algorithm alg = RooUnfolding::kPoisson;
    
    // Define the regularisation parameter.
    Double_t tau = 0.000001;

    //  Getan unfolding function.
    RooUnfoldFunc* unfoldFunc = (RooUnfoldFunc*)spec.makeFunc(alg, tau);

    // Instantiate a RooUnfold object with RooFitHist as template type.
    const RooUnfoldT<RooUnfolding::RooFitHist,RooUnfolding::RooFitHist>* unfold = unfoldFunc->unfolding();

    // Unfold.
    TVectorD unfoldedVec = unfold->Vunfold();

    // Save the unfolded distribution.
    results.push_back(unfoldedVec);

    delete unfoldFunc;
  }

  // Create a vector for the unfolded variations.
  TVectorD varup(results.at(0).GetNrows());
  TVectorD vardown(results.at(0).GetNrows());

  // Quadratically sum the differences between the unfolded variations and 
  // the unfolded nominal reco distribution.
  for (int i = 0; i < varup.GetNrows(); i++){
    varup(i) = (results.at(0)(i) - results.at(1)(i))*(results.at(0)(i) - results.at(1)(i)) + (results.at(0)(i) - results.at(3)(i))*(results.at(0)(i) - results.at(3)(i)) + (results.at(0)(i) - results.at(5)(i))*(results.at(0)(i) - results.at(5)(i));
    vardown(i) = (results.at(0)(i) - results.at(2)(i))*(results.at(0)(i) - results.at(2)(i)) + (results.at(0)(i) - results.at(4)(i))*(results.at(0)(i) - results.at(4)(i)) + (results.at(0)(i) - results.at(6)(i))*(results.at(0)(i) - results.at(6)(i));
  }

  TFile output("responsetemplates.root","RECREATE");

  TH1D* result = (TH1D*)truthTestHist->Clone();
  TH1D* unfolded = (TH1D*)truthTestHist->Clone();

  for (int i = 0; i < varup.GetNrows(); i++){

    // Symmetrize the errors and save in histogram.
    result->SetBinContent(i+1, (sqrt(varup(i)) + sqrt(vardown(i)))/2 );

    // Get the unfolded data.
    unfolded->SetBinContent(i+1, results.at(0)(i));
    unfolded->SetBinError(i+1, result->GetBinContent(i+1));
  }

  // Save the results.
  unfolded->Write("unfolded");
  truthTestHist->Write("truth");
  result->Write("errors");

  output.Close();
  input.Close();

}
