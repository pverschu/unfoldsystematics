## Unfolding with Systematics

# Introduction
There are several ways to propagate uncertainties onto an unfolded distribution. This repository contains 5 different methods:

- Toy MC on reconstructed distributions
- Toy MC on response matrix
- Unfolding reconstructed distribution templates
- Unfolding response matrix templates
- Full Likelihood unfolding

The unfolding is done with the ROOT-based library RooUnfold: https://gitlab.cern.ch/pverschu/RooUnfold