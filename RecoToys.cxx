

void RecoToys() {

  //std::string inputFile("input_bimodal.root");
  std::string inputFile("input_exp.root");

  // Define the number of sampled nuisance parameter values i.e. toys.
  Int_t nToys = 1000;

  TFile input(inputFile.c_str());

  TH1D* truthHist = (TH1D*)input.Get("truthnom");
  TH1D* recoHist = (TH1D*)input.Get("reconom");
  TH1D* recoBkgHist = (TH1D*)input.Get("reco_bkgnom");

  TH1D* dataHist = (TH1D*)input.Get("dataHist");
  TH1D* truthTestHist = (TH1D*)input.Get("truthTestHist");
  TH2D* respHist = (TH2D*)input.Get("responsenom");

  TH1D* reco_1_up = (TH1D*)input.Get("recoNP1up");
  TH1D* reco_1_down = (TH1D*)input.Get("recoNP1down");

  TH1D* reco_2_up = (TH1D*)input.Get("recoNP2up");
  TH1D* reco_2_down = (TH1D*)input.Get("recoNP2down");

  TH1D* reco_3_up = (TH1D*)input.Get("recoNP3up");
  TH1D* reco_3_down = (TH1D*)input.Get("recoNP3down");

  // Define a vector to save the unfolded distributions in.
  std::vector<TVectorD> results;

  // Define a RooRealVar for each nuisance parameter.
  RooRealVar NP1("np1","np1",0,-10,10);
  RooRealVar NP2("np2","np2",0,-10,10);
  RooRealVar NP3("np3","np3",0,-10,10);
  RooArgList NPs;
  NPs.add(NP1);
  NPs.add(NP2);
  NPs.add(NP3);
  
  // Create linear interpolation variables for each reco bin.
  std::vector<RooStats::HistFactory::LinInterpVar*> recoFuncs;

  for (int i = 0; i < recoHist->GetNbinsX(); i++){

    std::string reconame("reco_");
    reconame.append(std::to_string(i));

    std::vector<double> dn_vals;
    std::vector<double> up_vals;

    dn_vals.push_back(reco_1_down->GetBinContent(i+1));
    dn_vals.push_back(reco_2_down->GetBinContent(i+1));
    dn_vals.push_back(reco_3_down->GetBinContent(i+1));
    up_vals.push_back(reco_1_up->GetBinContent(i+1));
    up_vals.push_back(reco_2_up->GetBinContent(i+1));
    up_vals.push_back(reco_3_up->GetBinContent(i+1));

    RooStats::HistFactory::LinInterpVar* recoFunc = new RooStats::HistFactory::LinInterpVar(reconame.c_str(),reconame.c_str(),NPs,recoHist->GetBinContent(i+1),dn_vals,up_vals);
    
    recoFuncs.push_back(recoFunc);
  }

  Int_t n_reco_bins = dataHist->GetNbinsX();
  Int_t n_truth_bins = truthHist->GetNbinsX();
  
  // Throw toys.
  for (int t = 0; t < nToys+1; t++){

    TH1D *pseudoData;

    // First unfold the data.
    if (t == 0){
      pseudoData = dataHist;

    // Generate pseudo data.
    } else {
      pseudoData = (TH1D*)dataHist->Clone();
      
      // Sample a new NP value from a normal distribution.
      NP1.setVal(gRandom->Gaus(0,1));
      NP2.setVal(gRandom->Gaus(0,1));
      NP3.setVal(gRandom->Gaus(0,1));

      // Get the interpolated value of each reco bin.
      for (int i = 0; i < dataHist->GetNbinsX(); i++){
	pseudoData->SetBinContent(i+1,recoFuncs.at(i)->getVal());
      }
    }

    // Make a spectator object.
    RooUnfoldSpec* spec;
    if (t == 0){
      spec = new RooUnfoldSpec("unfold","unfold",truthHist,"obs_truth",recoHist,"obs_reco",respHist,recoBkgHist,pseudoData,false,0.0005);      
    } else {
      spec = new RooUnfoldSpec("unfold","unfold",truthHist,"obs_truth",recoHist,"obs_reco",respHist,NULL,pseudoData,false,0.0005);
    }

    
    // Define the unfolding algorithm.
    RooUnfolding::Algorithm alg = RooUnfolding::kPoisson;
    
    // Define the regularisation parameter.
    Double_t tau = 0.000001;

    // Get an unfolding function.
    RooUnfoldFunc* unfoldFunc = (RooUnfoldFunc*)spec->makeFunc(alg, tau);

    // Instantiate a RooUnfold object with RooFitHist as template type.
    const RooUnfoldT<RooUnfolding::RooFitHist,RooUnfolding::RooFitHist>* unfold = unfoldFunc->unfolding();

    // Unfold.
    TVectorD unfoldedVec = unfold->Vunfold();

    // Save the unfolded distribution.
    results.push_back(unfoldedVec);

    if (t > 0){
      delete pseudoData;
    }
    
    delete unfoldFunc;
    delete spec;
  }

  TFile output("recotoys.root","RECREATE");

  TH1D* result = (TH1D*)truthTestHist->Clone();
  TH1D* unfolded = (TH1D*)truthTestHist->Clone();

  //! Loop over the unfolded results.
  for (int i=0 ; i<truthHist->GetNbinsX() ; ++i) {
    
    double sum = 0;
 
    unfolded->SetBinContent(i+1, results[0][i]);

    for (int j=0 ; j<nToys ; ++j) {
      sum += results[j+1][i];
    }
    double mu = sum/nToys;
    double sum2 = 0;
    for (int j=0 ; j<nToys ; ++j) {
      sum2 += (results[j+1][i] - mu)*(results[j+1][i] - mu);
    }

    // Take the sqrt of the sample variance as the error.
    result->SetBinContent(i+1,sqrt(sum2/(nToys-1)));
    unfolded->SetBinError(i+1, sqrt(sum2/(nToys-1)));
  }

  // Save the results.
  unfolded->Write("unfolded");
  truthTestHist->Write("truth");
  result->Write("errors");

  output.Close();
  input.Close();

}
