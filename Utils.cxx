
TMatrixD Gmatrix(Int_t dim){
  
  TMatrixD G(dim, dim);

  for (int i = 2; i < dim-2; i++){
    G(i,i) = 6;
    G(i,i+1) = -4;
    G(i,i-1) = -4;
    G(i+1,i) = -4;
    G(i-1,i) = -4;
    G(i,i+2) = 1;
    G(i,i-2) = 1;
    G(i+2,i) = 1;
    G(i-2,i) = 1;
  }
      
  G(0,0) = 1;
  G(dim-1,dim-1) = 1;

  G(1,1) = 5;
  G(dim-2,dim-2) = 5;
  
  G(0,1) = -2;
  G(1,0) = -2;
  G(dim-1,dim-2) = -2;
  G(dim-2,dim-1) = -2;

  return G;
}


void makeAMatrix(TMatrixD& A, TMatrixD& R, TVectorD& nu, TVectorD& n, Double_t tau, Int_t reg_func){
  
  A.ResizeTo(R.GetNcols(), R.GetNcols());

  TMatrixD G = Gmatrix(R.GetNcols());

  for (int i = 0; i < A.GetNrows(); i++){
    for (int j = 0; j < A.GetNcols(); j++){
      
      Double_t d2logLdmu2 = 0;
      for (int k = 0; k < R.GetNrows(); k++){
	d2logLdmu2 += R(k,i)*R(k,j)*n(k)/(nu(k)*nu(k));
      }
      if (reg_func == 1){
	A(i,j) = d2logLdmu2 + 2*tau*G(i,j);
      } else {
	if (i==j){
	  A(i,j) = d2logLdmu2 + tau;
	} else {
	  A(i,j) = d2logLdmu2;
	}
      }
    }
  }
}

void makeAMatrix(TMatrixD& A, TMatrixD& R, std::vector<std::vector<std::vector<RooDerivative*>>>& dRdThetas, std::vector<std::vector<Double_t>>& dNudThetas, std::vector<std::vector<Double_t>>& d2Nud2Thetas, std::vector<Double_t>& sigma_m, TVectorD& nu, TVectorD& n, Double_t tau){
  
  A.ResizeTo(R.GetNcols() + dRdThetas.at(0).at(0).size(), R.GetNcols() + dRdThetas.at(0).at(0).size());
  
  TMatrixD G = Gmatrix(R.GetNcols());

  for (int i = 0; i < A.GetNrows(); i++){
    for (int j = 0; j < A.GetNcols(); j++){
      
      if ( i < R.GetNcols() && j < R.GetNcols()){

	Double_t d2logLd2mu = 0;
	for (int k = 0; k < R.GetNrows(); k++){
	  d2logLd2mu += R(k,i)*R(k,j)*n(k)/(nu(k)*nu(k));
	}
	A(i,j) = d2logLd2mu + 2*tau*G(i,j);
     
      } else if( i < R.GetNcols() && j >= R.GetNcols() ){

	Double_t d2logLdmudtheta = 0;
	for (int k = 0; k < R.GetNrows(); k++){
	  d2logLdmudtheta += R(k,i)*(1 + (dNudThetas.at(k).at(j - R.GetNcols())) * n(k) / (nu(k)*nu(k)));
	  d2logLdmudtheta += (1 - n(k)/(nu(k)*nu(k)))*((dRdThetas.at(k).at(i).at(j - R.GetNcols()))->getVal());
	}
	A(i,j) = d2logLdmudtheta;
	
      } else if ( i >= R.GetNcols() && j < R.GetNcols() ){

	Double_t d2logLdmudtheta = 0;
	for (int k = 0; k < R.GetNrows(); k++){
	  d2logLdmudtheta += R(k,j)*(1 + (dNudThetas.at(k).at(i - R.GetNcols())) * n(k) / (nu(k)*nu(k)));
	  d2logLdmudtheta += (1 - n(k)/nu(k))*((dRdThetas.at(k).at(j).at(i - R.GetNcols()))->getVal());
	}
	A(i,j) = d2logLdmudtheta;	
      } else {

	Double_t d2logLd2theta = 0;
	for (int k = 0; k < R.GetNrows(); k++){

	  d2logLd2theta += (1 + (dNudThetas.at(k).at(j - R.GetNcols()))*n(k)/(nu(k)*n(k)))*(dNudThetas.at(k).at(i - R.GetNcols()));
	  if (i == j){
	    d2logLd2theta += (1 - n(k)/(nu(k)*nu(k)))*(d2Nud2Thetas.at(k).at(j-R.GetNcols()));
	  } else {
	    d2logLd2theta += (1 - n(k)/(nu(k)*nu(k)))*(dNudThetas.at(k).at(j-R.GetNcols()))*(dNudThetas.at(k).at(i-R.GetNcols()));
	  }
	}
	if (i == j){
	  d2logLd2theta += 1/(sigma_m.at(i - R.GetNcols())*sigma_m.at(i - R.GetNcols()));
	}
	A(i,j) = d2logLd2theta;	
      }
    }
  }
}


void makeBMatrix(TMatrixD& B, TMatrixD& R, TVectorD& nu){

  Int_t nr_truth_bins = R.GetNcols();
  Int_t nr_reco_bins = R.GetNrows();

  B.ResizeTo(nr_truth_bins, nr_reco_bins);

  for (int i = 0; i < nr_truth_bins; i++){
    for (int j = 0; j < nr_reco_bins; j++){
      B(i,j) = -R(j,i) / nu(j);
    }
  }
}

void makeBMatrix(TMatrixD& B, TMatrixD& R, std::vector<std::vector<std::vector<RooDerivative*>>>& dRdThetas, TVectorD& nu, TVectorD& mu, std::vector<Double_t>& sigma_m){
  
  Int_t nr_truth_bins = mu.GetNrows();
  Int_t nr_NPs = dRdThetas.at(0).at(0).size();
  Int_t nr_params = nr_truth_bins + nr_NPs;
  Int_t nr_meas_bins = R.GetNrows() + nr_NPs;

  B.ResizeTo(nr_params, nr_meas_bins);
  
  TVectorD dRdThetaMu(nr_meas_bins - nr_NPs);

  for (int i  = 0; i < nr_params; i++){
    for (int j = 0; j < nr_meas_bins; j++){
      
      if (i < nr_truth_bins && j < R.GetNrows()){
	B(i,j) = - R(j,i) / nu(j);
	//B(i,j) = - R(j,i) / (R*mu)(j);
      }

      if (i < nr_truth_bins && j >= R.GetNrows()){
	B(i,j) = 0;
      }

      if (i >= nr_truth_bins && j < R.GetNrows()){
	for (int k = 0; k < dRdThetas.at(0).size(); k++){
	  dRdThetaMu(j) += (dRdThetas.at(j).at(k).at(i-nr_truth_bins)->getVal())*mu(k);
	}
	//B(i,j) = -dRdThetaMu(j) / (R*mu)(j);
	B(i,j) = -dRdThetaMu(j) / nu(j);
      }

      if (i >= nr_truth_bins && j >= R.GetNrows()){
	if (i==j){
	  B(i,j) = -1/(sigma_m.at(i-nr_truth_bins)*sigma_m.at(i-nr_truth_bins));
	} else {
	  B(i,j) = 0;
	}
      }
    }
  }
}


void evaluateResponseFunction(TMatrixD& response, std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>>& ResponseFunctions){
  
  response.ResizeTo(ResponseFunctions.size(), ResponseFunctions.at(0).size());

  for (int i = 0; i < ResponseFunctions.size(); i++){
    for (int j = 0; j < ResponseFunctions.at(0).size(); j++){
      response(i,j) = ResponseFunctions.at(i).at(j)->getVal();
    }
  }
}

void getNuDerivatives(std::vector<std::vector<Double_t>>& dNudThetas, std::vector<std::vector<Double_t>>& d2Nud2Thetas, std::vector<std::vector<std::vector<RooDerivative*>>>& dRdThetas, std::vector<std::vector<std::vector<RooDerivative*>>>& d2Rd2Thetas, TVectorD& mu){
  
  for (int i = 0; i < dRdThetas.size(); i++){
    
    std::vector<Double_t> dNudTheta_row;
    std::vector<Double_t> d2Nud2Theta_row;

    for (int j = 0; j < dRdThetas.at(0).at(0).size(); j++){
      Double_t dNudTheta = 0;
      Double_t d2Nud2Theta = 0;

      for (int k = 0; k < dRdThetas.at(0).size(); k++){
	dNudTheta += (dRdThetas.at(i).at(k).at(j)->getVal())*mu(k);
	d2Nud2Theta += (d2Rd2Thetas.at(i).at(k).at(j)->getVal())*mu(k);
      }
      dNudTheta_row.push_back(dNudTheta);
      d2Nud2Theta_row.push_back(d2Nud2Theta);
    }
    dNudThetas.push_back(dNudTheta_row);
    d2Nud2Thetas.push_back(d2Nud2Theta_row);
  }
}


void getdNudTheta(std::vector<TVectorD>& dNudThetas, std::vector<TMatrixD>& d2Nud2Thetas, std::vector<TMatrixD>& dRdThetas, std::vector<std::vector<TMatrixD>>& d2Rd2Thetas, TVectorD& mu){
  
  for (int i = 0; i < dRdThetas.size(); i++){
    
    TVectorD dNudTheta(dRdThetas.at(0).GetNrows());


    for (int j = 0; j < dRdThetas.at(0).GetNrows(); j++){   
      for (int k = 0; k < dRdThetas.at(0).GetNcols(); k++){
	dNudTheta(j) += (dRdThetas.at(i)(j,k))*mu(k);
      }
    }
    dNudThetas.push_back(dNudTheta);
  }

  for (int i = 0; i < dRdThetas.at(0).GetNrows(); i++){
    
    TMatrixD d2Nud2ThetaM(dRdThetas.size(), dRdThetas.size());
    
    for (int j = 0; j < dRdThetas.size(); j++){
      for (int k = 0; k < dRdThetas.size(); k++){
	
	Double_t d2Nud2Theta = 0;
	for (int l = 0; l < d2Rd2Thetas.at(0).size(); l++){
	  d2Nud2Theta += (d2Rd2Thetas.at(i).at(l)(j,k))*mu(l);
	}
	d2Nud2ThetaM(j,k) = d2Nud2Theta;
	
      }
      d2Nud2Thetas.push_back(d2Nud2ThetaM);
    }
  }
}

void getRDerivatives(std::vector<std::vector<std::vector<RooDerivative*>>>& first, std::vector<std::vector<std::vector<RooDerivative*>>>& second, std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>>& responseFunctions, RooArgList& NPs){
  
  for (int i = 0; i < responseFunctions.size(); i++){
    std::vector<std::vector<RooDerivative*>> der_row;
    std::vector<std::vector<RooDerivative*>> der2_row;
    for (int j = 0; j < responseFunctions.at(0).size(); j++){
      std::vector<RooDerivative*> der_col;
      std::vector<RooDerivative*> der2_col;

      Int_t k = 0;
      while(NPs.at(k)){
	RooDerivative* first_ij = responseFunctions.at(i).at(j)->derivative(*(RooRealVar*)NPs.at(k),1);
	RooDerivative* second_ij = responseFunctions.at(i).at(j)->derivative(*(RooRealVar*)NPs.at(k),2);
	der_col.push_back(first_ij);
	der2_col.push_back(second_ij);
	k++;
      }
      der_row.push_back(der_col);
      der2_row.push_back(der2_col);
    }
    first.push_back(der_row);
    second.push_back(der2_row);
  }
}


void getdRdTheta(std::vector<TMatrixD>& dRdThetas, std::vector<std::vector<TMatrixD>>& d2Rd2Thetas, RooArgList& NPs, std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>>& responseFunctions){
  
  // Save the final fit value of the nuisance parameters.
  Double_t theta_0 = ((RooRealVar*)NPs.at(0))->getVal();
  Double_t theta_1 = ((RooRealVar*)NPs.at(1))->getVal();

  // Set the nuisance parameters to their nominal value.
  ((RooRealVar*)NPs.at(0))->setVal(0);
  ((RooRealVar*)NPs.at(1))->setVal(0);

  // Create matrices that will contain the up/down variation of the nuisance parameter.
  TMatrixD resp_nom(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta0_up(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta1_up(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta0_down(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta1_down(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta_upup(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta_downdown(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta_updown(responseFunctions.size(), responseFunctions.at(0).size());
  TMatrixD resp_theta_downup(responseFunctions.size(), responseFunctions.at(0).size());
  

  // Create matrices which will contain the derivatives.
  TMatrixD dRdTheta0(resp_nom.GetNrows(), resp_nom.GetNcols());
  TMatrixD dRdTheta1(resp_nom.GetNrows(), resp_nom.GetNcols());

  // Create matrices which will contain the second derivatives.
  

  // TMatrixD d2Rd2Theta0(resp_nom.GetNrows(), resp_nom.GetNcols());
  // TMatrixD d2Rd2Theta1(resp_nom.GetNrows(), resp_nom.GetNcols());

  // The dx used in the linear approximation of the derivatives.
  Double_t dTheta0 = 1;
  Double_t dTheta1 = 1;
  
  // Get the nominal response matrix.
  evaluateResponseFunction(resp_nom, responseFunctions);

  // Depending on the final fit value, the variation used to
  // linearly approximate the derivative.

  ((RooRealVar*)NPs.at(0))->setVal(1);
  evaluateResponseFunction(resp_theta0_up, responseFunctions); 
  ((RooRealVar*)NPs.at(0))->setVal(-1);
  evaluateResponseFunction(resp_theta0_down, responseFunctions); 

  ((RooRealVar*)NPs.at(0))->setVal(0);
  ((RooRealVar*)NPs.at(1))->setVal(1);
  evaluateResponseFunction(resp_theta1_up, responseFunctions); 
  ((RooRealVar*)NPs.at(1))->setVal(-1);
  evaluateResponseFunction(resp_theta1_down, responseFunctions); 

  ((RooRealVar*)NPs.at(0))->setVal(-1);
  evaluateResponseFunction(resp_theta_downdown, responseFunctions); 

  ((RooRealVar*)NPs.at(0))->setVal(1);
  evaluateResponseFunction(resp_theta_downup, responseFunctions);

  ((RooRealVar*)NPs.at(1))->setVal(1);
  evaluateResponseFunction(resp_theta_upup, responseFunctions); 

  ((RooRealVar*)NPs.at(0))->setVal(-1);
  evaluateResponseFunction(resp_theta_updown, responseFunctions); 
  

  ((RooRealVar*)NPs.at(0))->setVal(0);
  ((RooRealVar*)NPs.at(1))->setVal(0);

  // Calculate the derivatives with a linear approximation.
  // dR/dTheta = (R_up - R_nom)/dTheta for theta > 0
  // dR/dTheta = (R_nom - R_down)/dTheta for theta < 0
  // dTheta = 1 by construction of the response matrix variations.
  for (int i = 0; i < responseFunctions.size(); i++){
    std::vector<TMatrixD> d2Rd2ThetaV;
    for (int j = 0; j < responseFunctions.at(0).size(); j++){

      if (theta_0 > 0){
      	dRdTheta0(i,j) = resp_theta0_up(i,j) - resp_nom(i,j);
      } else if (theta_0 < 0) {
      	dRdTheta0(i,j) = resp_nom(i,j) - resp_theta0_down(i,j);
      } else {
	dRdTheta0(i,j) = (resp_theta0_up(i,j) - resp_theta0_down(i,j)) / 2;	 
      }
      
      if (theta_1 > 0){
      	dRdTheta1(i,j) = resp_theta1_up(i,j) - resp_nom(i,j);
      } else if (theta_1 < 0) {
      	dRdTheta1(i,j) = resp_nom(i,j) - resp_theta1_down(i,j);
      } else {
      	dRdTheta1(i,j) = (resp_theta1_up(i,j) - resp_theta1_down(i,j))/2;
      }
      TMatrixD d2Rd2Theta(NPs.size(), NPs.size());

      d2Rd2Theta(0,0) = resp_theta0_up(i,j) - 2*resp_nom(i,j) + resp_theta0_down(i,j);
      d2Rd2Theta(1,1) = resp_theta1_up(i,j) - 2*resp_nom(i,j) + resp_theta1_down(i,j);

      Double_t cross_term = (resp_theta_upup(i,j) - resp_theta_downup(i,j) - resp_theta_updown(i,j) + resp_theta_downdown(i,j))/4;
      d2Rd2Theta(0,1) = cross_term;
      d2Rd2Theta(1,0) = cross_term;
      d2Rd2ThetaV.push_back(d2Rd2Theta);
    }
    d2Rd2Thetas.push_back(d2Rd2ThetaV);
  }
 
  // Save the matrices with the derivatives.
  dRdThetas.push_back(dRdTheta0);
  dRdThetas.push_back(dRdTheta1);

  // Set the nuisance parameters back to their final fit value.
  ((RooRealVar*)NPs.at(0))->setVal(theta_0);
  ((RooRealVar*)NPs.at(1))->setVal(theta_1);
}


void fillVector(TVectorD& vec, RooArgList truthVars){
  for (int i = 0; i < truthVars.size(); i++){
    vec(i) = ((RooRealVar*)truthVars.at(i))->getVal();
  }
}


RooFormulaVar makeTikhonovFormula(RooArgList& mu, Double_t tau){

  std::string formulaString;
  formulaString.append(std::to_string(tau));
  formulaString.append(" * (");

  RooArgList S;
  
  Int_t i = 0;
  while(mu.at(i)){
    S.add(*(mu.at(i)));
    i++;
  }

  i = 0;
  while(mu.at(i+2)){

    if (i!=0) formulaString.append("+");
    formulaString.append("(-@");
    formulaString.append(std::to_string(i));
    formulaString.append("+2*");
    formulaString.append("@");
    formulaString.append(std::to_string(i+1));
    formulaString.append("-@");
    formulaString.append(std::to_string(i+2));

    formulaString.append(")*(");

    formulaString.append("-@");
    formulaString.append(std::to_string(i));
    formulaString.append("+2*");
    formulaString.append("@");
    formulaString.append(std::to_string(i+1));
    formulaString.append("-@");
    formulaString.append(std::to_string(i+2));
    formulaString.append(")");
    
    i++;
  }

  formulaString.append(")");
  
  return RooFormulaVar("S",formulaString.c_str(),S);
  
}

RooFormulaVar makeRefFormula(RooArgList& mu, TH1D* mu_ref, Double_t tau){

  std::string formulaString;
  formulaString.append(std::to_string(0.5));
  formulaString.append(" * ");
  formulaString.append(std::to_string(tau));
  formulaString.append(" * (");

  RooArgList S;
  
  Int_t i = 0;
  while(mu.at(i)){
    S.add(*(mu.at(i)));
    formulaString.append("(@");
    formulaString.append(std::to_string(i));
    // formulaString.append(" * @");
    // formulaString.append(std::to_string(i));
    formulaString.append(" - ");
    formulaString.append(std::to_string(mu_ref->GetBinContent(i+1)));
    // formulaString.append(" * ");
    // formulaString.append(std::to_string(mu_ref->GetBinContent(i+1)));
    formulaString.append(" ) *");

    formulaString.append("(@");
    formulaString.append(std::to_string(i));
    // formulaString.append(" * @");
    // formulaString.append(std::to_string(i));
    formulaString.append(" - ");
    formulaString.append(std::to_string(mu_ref->GetBinContent(i+1)));
    // formulaString.append(" * ");
    // formulaString.append(std::to_string(mu_ref->GetBinContent(i+1)));
    formulaString.append(" ) +");

    i++;
  }

  formulaString.append(" 0)");
  
  return RooFormulaVar("S",formulaString.c_str(),S);
  
}

void getResponse(TMatrixD& Rij, TH2D* respHist, TH1D* truthHist){
  
  for (int i = 0; i < respHist->GetNbinsX(); i++){    
    for (int j = 0; j < respHist->GetNbinsY(); j++){
      Rij(i,j) = respHist->GetBinContent(i+1, j+1) / truthHist->GetBinContent(j+1);
    }
  }
}


void getResponseFunctions(RooArgList& NPs, TH2D* nom_hist, TH1D* nom_norm, std::vector<TH2D*>& up_hists, std::vector<TH2D*>& dn_hists, std::vector<TH1D*> up_norm_hists, std::vector<TH1D*> dn_norm_hists, std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>>& ResponseFunctions){

  TMatrixD nom(nom_hist->GetNbinsX(), nom_hist->GetNbinsY());
  getResponse(nom,nom_hist,nom_norm);
  
  std::vector<TMatrixD> up;
  std::vector<TMatrixD> dn;

  for (int i = 0; i < NPs.size(); i++){
    TMatrixD response_up(nom_hist->GetNbinsX(), nom_hist->GetNbinsY());
    TMatrixD response_dn(nom_hist->GetNbinsX(), nom_hist->GetNbinsY());

    getResponse(response_up, up_hists.at(i), up_norm_hists.at(i));
    getResponse(response_dn, dn_hists.at(i), dn_norm_hists.at(i));

    up.push_back(response_up);
    dn.push_back(response_dn);
  }
	 
  for (int i = 0; i < nom.GetNrows(); i++){
    
    std::vector<RooStats::HistFactory::LinInterpVar*> ResponseFunctionsRow;
    for (int j = 0; j < nom.GetNcols(); j++){
      std::string rname("R_");
      std::vector<double> dn_vals;
      std::vector<double> up_vals;

      for(int k = 0; k < NPs.size(); k++){
	dn_vals.push_back((dn.at(k))(i,j));
	up_vals.push_back((up.at(k))(i,j));
      }
      rname.append(std::to_string(i));
      rname.append(std::to_string(j));

      RooStats::HistFactory::LinInterpVar* RFunction = new RooStats::HistFactory::LinInterpVar(rname.c_str(),rname.c_str(),NPs,nom(i,j),dn_vals,up_vals);
      ResponseFunctionsRow.push_back(RFunction);
    }
    ResponseFunctions.push_back(ResponseFunctionsRow);
  }
}


void getNPDerivativeMatrices(TH2D* nom_hist, std::vector<TH2D*>& up_hists, std::vector<TH2D*>& down_hists, std::vector<std::vector<std::vector<RooRealVar*>>>& NPDerMat){
  
  if (up_hists.size() != down_hists.size()){
    std::cout << "Error: number of varied response histogram templates not the same for up as down." << std::endl;
    return;
  }

  // Normalize the histograms to a response matrix.
  for (int i = 0; i < nom_hist->GetNbinsY(); i++){

    double nom_sum = 0;
    std::vector<double> NP_up_sums;
    std::vector<double> NP_down_sums;
    for(int NP_i = 0; NP_i < up_hists.size(); NP_i++){
      NP_up_sums.push_back(0);
      NP_down_sums.push_back(0);
    }

    // Sum over all truth bins.
    for (int j = 0; j < nom_hist->GetNbinsX() + 2; j++){
      nom_sum += nom_hist->GetBinContent(j, i+1);

      for(int NP_i = 0; NP_i < up_hists.size(); NP_i++){
	NP_up_sums.at(NP_i) = NP_up_sums.at(NP_i) + up_hists.at(NP_i)->GetBinContent(j,i+1);
	NP_down_sums.at(NP_i) = NP_down_sums.at(NP_i) + down_hists.at(NP_i)->GetBinContent(j,i+1);
      }      
    }
    
    // Normalize. 
    for (int j = 0; j < nom_hist->GetNbinsX(); j++){   
      nom_hist->SetBinContent(j+1,i+1, nom_hist->GetBinContent(j+1,i+1) / nom_sum );

      for(int NP_i = 0; NP_i < up_hists.size(); NP_i++){
	up_hists.at(NP_i)->SetBinContent(j+1, i+1, up_hists.at(NP_i)->GetBinContent(j+1, i+1) / NP_up_sums.at(NP_i));
	down_hists.at(NP_i)->SetBinContent(j+1, i+1, down_hists.at(NP_i)->GetBinContent(j+1, i+1) / NP_down_sums.at(NP_i));
      }      
    }
  }

    
  // Calculate the derivatives with finite differences.
  for (int i = 0; i < nom_hist->GetNbinsX(); i++){
    std::vector<std::vector<RooRealVar*>> input_col;
    for (int j = 0; j < nom_hist->GetNbinsY(); j++){
      std::vector<RooRealVar*> vec_der;
      for (int NP_i = 0; NP_i < up_hists.size(); NP_i++){

	Double_t dRdTheta =  (up_hists.at(NP_i)->GetBinContent(i+1, j+1) - down_hists.at(NP_i)->GetBinContent(i+1, j+1))/2;
	
	std::string rname("dR_");
	rname.append(std::to_string(i));
	rname.append(std::to_string(j));
	rname.append("/dTheta_");
	rname.append(std::to_string(NP_i));
	
	RooRealVar* NPDer = new RooRealVar(rname.c_str(),rname.c_str(),dRdTheta);
	vec_der.push_back(NPDer);
      }
      input_col.push_back(vec_der);
    }
    NPDerMat.push_back(input_col);
  }  
  
}

// This builds the nu(reco bin count) as function of the truth bin counts and
// nuisance parameters.
void makeNuFormulaWithNPs(RooArgList& mu, std::vector<RooFormulaVar>& nuFormulas, std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>>& responseFunctions, TH1D*& bkg){
  
  for (int i = 0; i < responseFunctions.size(); i++){

    
    Int_t param_count = 0;
    
    RooArgList nu;

    std::vector<RooFormulaVar> Rrow;
    
    // A string is constructed that represents the sum of response
    // matrix elements and truth bins. Floating parameters are denoted
    // with @ and are denoted with the index of the RooRealVar in the
    // RooArgList that is passed to RooFormulaVar.
    // nu_{i} = Sum_{j} R(theta)_{ij} * mu_{j} * eff_{j}
    //        = R(theta)_i1 * mu_1 * eff_1 + ...
    //        = ( R(theta_nom)_i1 + theta_1 * dR(theta_1)_i1/dtheta_1 + ...) * mu_1 * eff_1 + ...
    //        = ( R(theta_nom)_i1 * mu_1 * eff_1 + theta_1 * dR(theta_1)_i1/dtheta_1 * mu_1 * eff_1 + theta_2 * dR(theta_2)_i1/dtheta_2 * mu_1 * eff_1 + ...
    //        = ( @0 * @1 * 0.83 + @2 * @3 * @4 * 0.83 + @5 * @6 * @7 * 0.83 +...

    // Start of the formula string.
    std::string formulaString("");
    Int_t j = 0;
    while (mu.at(j)){

      RooArgList R;
      Int_t R_param_count = 0;
      std::string RFormulaString("");

      // std::string rname("dR_");
      // rname.append(std::to_string(i));
      // rname.append(std::to_string(j));
     
      // Convert the nominal response matrix element value to a string.
      formulaString.append("@");
      formulaString.append(std::to_string(param_count));
      param_count++;
      nu.add(*(responseFunctions.at(i).at(j)));

      // RFormulaString.append("@");
      // RFormulaString.append(std::to_string(R_param_count));
      // R_param_count++;
      // R.add(*R_nom);      

      // Multiply with truth bin.
      formulaString.append(" * ");
      formulaString.append("@");
      formulaString.append(std::to_string(param_count));
      param_count++;
      
      nu.add(*(mu.at(j)));

      formulaString.append(" + ");
      j++;
    }
    if(bkg){
      formulaString.append(std::to_string(bkg->GetBinContent(i+1)));
    } else {
      formulaString.append(std::to_string(0));
    }
    std::string nu_name("nu_formula_");
    nu_name.append(std::to_string(i));

    RooFormulaVar nu_formula(nu_name.c_str(), formulaString.c_str(), nu);
    nuFormulas.push_back(nu_formula);
  }
}


