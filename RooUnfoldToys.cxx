using namespace RooFit;

void RooUnfoldToys() {

  std::string file_path("input_exp.root");

  // Define the number of sampled nuisance parameter values i.e. toys.
  Int_t nToys = 1000;
  
  TFile file(file_path.c_str());

  TH1D* truthHist = (TH1D*)file.Get("truthnom");
  TH1D* recoHist = (TH1D*)file.Get("reconom");
  TH1D* recoBkgHist = (TH1D*)file.Get("reco_bkgnom");
  TH1D* dataHist = (TH1D*)file.Get("dataHist");
  TH1D* truthTestHist = (TH1D*)file.Get("truthTestHist");
  TH2D* respHist = (TH2D*)file.Get("responsenom");

  TH1D* truth_1_up = (TH1D*)file.Get("truthNP1up");
  TH1D* truth_1_down = (TH1D*)file.Get("truthNP1down");

  TH1D* truth_2_up = (TH1D*)file.Get("truthNP2up");
  TH1D* truth_2_down = (TH1D*)file.Get("truthNP2down");

  TH1D* truth_3_up = (TH1D*)file.Get("truthNP3up");
  TH1D* truth_3_down = (TH1D*)file.Get("truthNP3down");

  TH1D* reco_1_up = (TH1D*)file.Get("recoNP1up");
  TH1D* reco_1_down = (TH1D*)file.Get("recoNP1down");

  TH1D* reco_2_up = (TH1D*)file.Get("recoNP2up");
  TH1D* reco_2_down = (TH1D*)file.Get("recoNP2down");

  TH1D* reco_3_up = (TH1D*)file.Get("recoNP3up");
  TH1D* reco_3_down = (TH1D*)file.Get("recoNP3down");

  TH1D* reco_bkg_4_up = (TH1D*)file.Get("reco_bkgNP4up");
  TH1D* reco_bkg_4_down = (TH1D*)file.Get("reco_bkgNP4down");

  TH2D* response_1_up = (TH2D*)file.Get("responseNP1up");
  TH2D* response_1_down = (TH2D*)file.Get("responseNP1down");

  TH2D* response_2_up = (TH2D*)file.Get("responseNP2up");
  TH2D* response_2_down = (TH2D*)file.Get("responseNP2down");

  TH2D* response_3_up = (TH2D*)file.Get("responseNP3up");
  TH2D* response_3_down = (TH2D*)file.Get("responseNP3down");

  Int_t n_reco_bins = dataHist->GetNbinsX();
  Int_t n_truth_bins = truthHist->GetNbinsX();  

  // Make a spectator object.
  RooUnfoldSpec spec("unfold","unfold",truthHist,"obs_truth",recoHist,"obs_reco",respHist,recoBkgHist,dataHist,false,0.0005);
  
  // Register the systematics.
  spec.registerSystematic(RooUnfoldSpec::kResponse, "respreconp1", response_1_up, response_1_down);
  spec.registerSystematic(RooUnfoldSpec::kResponse, "respreconp2", response_2_up, response_2_down);
  spec.registerSystematic(RooUnfoldSpec::kResponse, "responsetruthnp3", response_3_up, response_3_down);
  //spec.registerSystematic(RooUnfoldSpec::kTruth, "responsetruthnp3", truth_3_up, truth_3_down);

  // Define the unfolding algorithm.
  RooUnfolding::Algorithm alg = RooUnfolding::kPoisson;
  
  // Define the regularisation parameter.
  Double_t tau = 0.000001;
  
  // Get an unfolding function.
  RooUnfoldFunc* unfoldFunc = (RooUnfoldFunc*)spec.makeFunc(alg, tau);
  
  // Instantiate a RooUnfold object with RooFitHist as template type.
  RooUnfoldT<RooUnfolding::RooFitHist,RooUnfolding::RooFitHist>* unfold = const_cast<RooUnfoldT<RooUnfolding::RooFitHist,RooUnfolding::RooFitHist>*>(unfoldFunc->unfolding());

  // Include systematics(kGammas = MC stats only, kAlphas = Theoretical and experimental only, kAll = All systematics)
  unfold->IncludeSystematics(RooUnfolding::kAll);

  // Unfold.
  TVectorD unfoldedVec = unfold->Vunfold();

  // Get the errors by throwing toys.
  unfold->SetNToys(nToys);
  TVectorD errorVec = unfold->EunfoldV(RooUnfolding::kErrorsToys);

  TH1D* result = (TH1D*)truthTestHist->Clone();
  TH1D* unfolded = (TH1D*)truthTestHist->Clone();
  TH1D* rel_err = (TH1D*)unfolded->Clone();

  //! Loop over the unfolded results.
  for (int i=0 ; i<truthHist->GetNbinsX(); ++i) {

    rel_err->SetBinContent(i+1, errorVec(i));
    unfolded->SetBinContent(i+1, unfoldedVec(i));
    unfolded->SetBinError(i+1, errorVec(i));

  }


  TFile output("roounfoldtoys.root","RECREATE");

  // Save the results.
  unfolded->Write("unfolded");
  truthTestHist->Write("truth");
  rel_err->Write("errors");

  output.Close();
  file.Close();

}
