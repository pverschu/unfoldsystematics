
#include <RooAbsReal.h>
#include <RooPoisson.h>
#include "Utils.cxx"
#include "RooStats/HistFactory/LinInterpVar.h"

using namespace RooFit;

  
void ClosedForm() {

  // Define the regularisation parameter.
  Double_t tau = 0.000001;

  // Define the mean and sigma of the Gaussian pdf of the nuisance parameters.
  Double_t m_1 = 0;
  Double_t m_2 = 0;
  Double_t m_3 = 0;
  std::vector<Double_t> sigma_m;
  sigma_m.push_back(1);
  sigma_m.push_back(1);
  sigma_m.push_back(1);

  // Include systematics or not.
  Bool_t sys = true;

  // Define the regularization function.(1=Tikhonov k=2, 2=Reference distribution)
  Int_t reg_func = 1;

  std::string inputFile("input_bimodal.root");

  TFile file(inputFile.c_str());

  TH1D* truthHist = (TH1D*)file.Get("truthnom");
  TH1D* recoHist = (TH1D*)file.Get("reconom");
  TH1D* recoBkgHist = (TH1D*)file.Get("reco_bkgnom");
  TH1D* dataHist = (TH1D*)file.Get("dataHist");
  TH1D* truthTestHist = (TH1D*)file.Get("truthTestHist");
  TH2D* respHist = (TH2D*)file.Get("responsenom");

  TH1D* truth_1_up = (TH1D*)file.Get("truthNP1up");
  TH1D* truth_1_down = (TH1D*)file.Get("truthNP1down");

  TH1D* truth_2_up = (TH1D*)file.Get("truthNP2up");
  TH1D* truth_2_down = (TH1D*)file.Get("truthNP2down");

  TH1D* truth_3_up = (TH1D*)file.Get("truthNP3up");
  TH1D* truth_3_down = (TH1D*)file.Get("truthNP3down");

  TH1D* reco_1_up = (TH1D*)file.Get("recoNP1up");
  TH1D* reco_1_down = (TH1D*)file.Get("recoNP1down");

  TH1D* reco_2_up = (TH1D*)file.Get("recoNP2up");
  TH1D* reco_2_down = (TH1D*)file.Get("recoNP2down");

  TH1D* reco_3_up = (TH1D*)file.Get("recoNP3up");
  TH1D* reco_3_down = (TH1D*)file.Get("recoNP3down");

  TH1D* reco_bkg_4_up = (TH1D*)file.Get("reco_bkgNP4up");
  TH1D* reco_bkg_4_down = (TH1D*)file.Get("reco_bkgNP4down");

  TH2D* response_1_up = (TH2D*)file.Get("responseNP1up");
  TH2D* response_1_down = (TH2D*)file.Get("responseNP1down");

  TH2D* response_2_up = (TH2D*)file.Get("responseNP2up");
  TH2D* response_2_down = (TH2D*)file.Get("responseNP2down");

  TH2D* response_3_up = (TH2D*)file.Get("responseNP3up");
  TH2D* response_3_down = (TH2D*)file.Get("responseNP3down");


  
  Int_t n_reco_bins = dataHist->GetNbinsX();
  Int_t n_truth_bins = truthHist->GetNbinsX();
  
  RooArgList dataBinVars;
  RooArgList recoBinVars;
  RooArgList truthBinVars;
  RooArgList pdfList;

  
  // Create a list with variables for all the truth bins.
  for (int i = 0; i < n_truth_bins; i++){

    std::string truthBinName("mu_");
    truthBinName.append(std::to_string(i));
    
    RooRealVar* truthBinVar = new RooRealVar(truthBinName.c_str(),truthBinName.c_str(),truthHist->GetBinContent(i+1),0,100000);
    truthBinVar->setError(0.1);
    
    truthBinVars.add(*truthBinVar);
  }
  
  // Each RooFormulaVar defines each reco bin in terms
  // of response matrix elements and truth bins i.e.
  // nu_{i} = Sum_{j=1} R_{ij}mu{j}
  std::vector<RooFormulaVar> nuFormulas;

  // A list with the nuisance parameter variables.
  RooArgList NPVars;

  // Create the variables for the nuisance parameters.
  RooRealVar* NP1;
  RooRealVar* NP2;
  RooRealVar* NP3;

  // If systematics need not to be included then set these 
  // parameters const and to 0.
  if (sys) {
    NP1 = new RooRealVar("theta0","theta0",0,-50,50);
    NP2 = new RooRealVar("theta1","theta1",0,-50,50);
    NP3 = new RooRealVar("theta2","theta2",0,-50,50);
  } else {
    NP1 = new RooRealVar("theta0","theta0",0);
    NP2 = new RooRealVar("theta1","theta1",0);
    NP3 = new RooRealVar("theta2","theta2",0);
  }

  NP1->setError(0.0001);
  NP2->setError(0.0001);
  NP3->setError(0.0001);

  NPVars.add(*NP1);
  NPVars.add(*NP2);
  NPVars.add(*NP3);

  std::vector<TH2D*>up_hists;
  std::vector<TH2D*>down_hists;
  std::vector<TH1D*>up_norm_hists;
  std::vector<TH1D*>down_norm_hists;

  up_hists.push_back(response_1_up);
  up_hists.push_back(response_2_up);
  up_hists.push_back(response_3_up);
  down_hists.push_back(response_1_down);
  down_hists.push_back(response_2_down);
  down_hists.push_back(response_3_down);

  up_norm_hists.push_back(truth_1_up);
  up_norm_hists.push_back(truth_2_up);
  up_norm_hists.push_back(truth_3_up);
  down_norm_hists.push_back(truth_1_down);
  down_norm_hists.push_back(truth_2_down);
  down_norm_hists.push_back(truth_3_down);

  std::vector<std::vector<RooStats::HistFactory::LinInterpVar*>> responseFunctions;
  std::vector<std::vector<std::vector<RooDerivative*>>> dRdThetas;
  std::vector<std::vector<std::vector<RooDerivative*>>> d2Rd2Thetas;

  getResponseFunctions(NPVars, respHist, truthHist, up_hists, down_hists, up_norm_hists, down_norm_hists, responseFunctions);
  getRDerivatives(dRdThetas, d2Rd2Thetas, responseFunctions, NPVars);
  
  TMatrixD response(n_reco_bins, n_truth_bins);
    
  // Create the formulas.
  makeNuFormulaWithNPs(truthBinVars, nuFormulas, responseFunctions, recoBkgHist);

  // Define the NLL constraint term formula that constrains on smoothness:
  // S(mu) = - tau * Sum_{i}^{M-2}(-mu_{i} + 2mu_{i+1} - mu_{i+2})^{2}
  RooFormulaVar Smu = makeTikhonovFormula(truthBinVars,tau);
  //RooFormulaVar Smu = makeRefFormula(truthBinVars,truthTestHist,tau);

  // Define the NLL constraint term formula that constrains on the difference between
  // the truth parameters and a truth benchmark:
  // S(mu) = tau * Sum_{i}^{M}(mu_{i} - muBench_{i})^{2}
  //RooFormulaVar Smu = makeTruthDiffConstr(truthBinVars,truthVec,tau);
  
  // Loop over all the truth bins and construct a Poisson pdf for each.
  for (int i = 0; i < n_reco_bins; i++){

    std::string pdfName("poisPdf_bin_");
    std::string dataBinName("n_");
    
    pdfName.append(std::to_string(i));
    dataBinName.append(std::to_string(i));

    // Create a variable for the data bin count.
    RooRealVar* dataBinVar = new RooRealVar(dataBinName.c_str(),dataBinName.c_str(),dataHist->GetBinContent(i+1));

    // Create a Poisson p.d.f. for the likelihood.
    RooPoisson* poisPdf = new RooPoisson(pdfName.c_str(),pdfName.c_str(),*dataBinVar,nuFormulas.at(i));

    dataBinVars.add(*dataBinVar);

    pdfList.add(*poisPdf);
  }

  // These values represent an auxiliary measurement or theoretical guess
  // with an uncertainty on it for each nuisance parameter.
  RooRealVar* NP1_mean = new RooRealVar("theta0_mean","theta0_mean",-50,50);
  RooRealVar* NP2_mean = new RooRealVar("theta1_mean","theta1_mean",-50,50);
  RooRealVar* NP3_mean = new RooRealVar("theta2_mean","theta2_mean",-50,50);
  RooRealVar* NP1_sig = new RooRealVar("theta0_sig","theta0_sig",sigma_m.at(0));
  RooRealVar* NP2_sig = new RooRealVar("theta1_sig","theta1_sig",sigma_m.at(1));
  RooRealVar* NP3_sig = new RooRealVar("theta2_sig","theta2_sig",sigma_m.at(2));
  
  // Add Gaussian constraint terms for the nuisance parameters.
  RooGaussian* theta0Constr = new RooGaussian("theta0pdf","theta0pdf",*NP1_mean,*((RooRealVar*)NPVars.at(0)),*NP1_sig);
  RooGaussian* theta1Constr = new RooGaussian("theta1pdf","theta1pdf",*NP2_mean,*((RooRealVar*)NPVars.at(1)),*NP2_sig);
  RooGaussian* theta2Constr = new RooGaussian("theta2pdf","theta2pdf",*NP3_mean,*((RooRealVar*)NPVars.at(2)),*NP3_sig);
  
  // Add the Gaussian constraint terms to the pdf list.
  pdfList.add(*theta0Constr);
  pdfList.add(*theta1Constr);
  pdfList.add(*theta2Constr);
  
  // Set the value for the auxiliary measurement.
  NP1_mean->setVal(m_1);
  NP2_mean->setVal(m_2);
  NP3_mean->setVal(m_3);
  
  // Add the auxiliary measurements to the data variables.
  dataBinVars.add(*NP1_mean);
  dataBinVars.add(*NP2_mean);
  dataBinVars.add(*NP3_mean);

  // Take the product of all pdfs.
  RooProdPdf LH("Likelihood","Likelihood",pdfList);
  
  // Make a dataset for all the data bin variables.
  RooDataSet in("indata","indata",dataBinVars);

  // Add this one datapoint.
  in.add(dataBinVars);

  // Create the negative log-likelihood.
  RooAbsReal* nll = LH.createNLL(in);

  // Add the constraint term to the NLL.
  RooFormulaVar nllConstr("constrNLL","@0 + @1",RooArgList(*nll,Smu));  
  
  // Create a minuit instance.
  RooMinuit *roomin = new RooMinuit(nllConstr);
  //RooMinuit *roomin = new RooMinuit(*nll);

  // Define the computational rigorousness:
  // (1=fast,2=slow but more accurate,3=intermediate)
  roomin->setStrategy(2);
  
  // Minimize.
  roomin->migrad();

  // Get the response matrix evaluated at the final fit values
  // of the nuisance parameters.
  evaluateResponseFunction(response, responseFunctions);

  TVectorD Nu(recoHist->GetNbinsX());
  TVectorD n(recoHist->GetNbinsX());
  TVectorD mu(truthHist->GetNbinsX());

  // Get the final fit values of the truth bin vars.
  fillVector(mu, truthBinVars);

  // Get the expected reco bins evaluated at the final fit values
  // of both the truth bin and nuisance parameters.
  for (int i = 0; i < recoHist->GetNbinsX(); i++){
    Nu(i) = nuFormulas.at(i).getVal();
    n(i) = dataHist->GetBinContent(i+1);
  }

  // Vector index = theta, TVector index = reco bin
  std::vector<std::vector<Double_t>> dNudThetas;

  // Vector index = reco bin, TMatrix indices = theta_i theta_j
  std::vector<std::vector<Double_t>> d2Nud2Thetas;

  // Get the derivatives of the response matrix w.r.t. the nuisance parameters
  // evaluated at the final fit values of the nuisance parameters.
  getNuDerivatives(dNudThetas, d2Nud2Thetas, dRdThetas, d2Rd2Thetas, mu);

  // Save the final fit results.
  RooFitResult* fitres = roomin->save();

  // Get the inverted hessian. In case of no regularization
  // this would be the covariance. However, in case of a 
  // regularized likelihood the unbiased RCF bound no longer holds. 
  // Denoted as A^{-1} in the supporting literature.
  TMatrixDSym inv_hess(fitres->covarianceMatrix());

  // Another second order derivative matrix denoted as B
  // in the supporting literature. All derivatives are
  // evaluated at the final values.
  TMatrixD B;
  TMatrixD A;

  makeBMatrix(B, response, dRdThetas, Nu, mu, sigma_m);
  makeAMatrix(A, response, dRdThetas, dNudThetas, d2Nud2Thetas, sigma_m, Nu, n, tau);

  A.Invert();  

  // Calculate the covariance matrix.
  TMatrixD C(A*B);
  TMatrixD CT(A*B);
  CT.T();

  // Get the measured covariance of the data. All the bins are taken to
  // be independently Poisson or Gaussian distributed. The diagonal elements
  // are therefore set to their expected value V(i,i) = delta(i,j) * nu_{i}.
  // This includes also auxiliary measurements which are set to their
  // respective sigma_{theta} of their Gaussian p.d.f.
  TMatrixD V;
  V.ResizeTo(recoHist->GetNbinsX() + NPVars.size(), recoHist->GetNbinsX() + NPVars.size());
  for (int i = 0; i < V.GetNrows(); i++){
    if (i < recoHist->GetNbinsX()){
      //V(i,i) = recoHist->GetBinContent(i+1);
      V(i,i) = Nu(i);
    } else {
      V(i,i) = sigma_m.at(i - recoHist->GetNbinsX()) * sigma_m.at(i - recoHist->GetNbinsX());
    }
  }
  
  TMatrixD VCT(V*CT);

  TMatrixD U(C*VCT);
  
  TFile output("closedform.root","RECREATE");

  TH1D* errors = (TH1D*)truthTestHist->Clone();
  TH1D* unfolded = (TH1D*)truthTestHist->Clone();

  for (int i = 0; i < unfolded->GetNbinsX(); i++){

    unfolded->SetBinContent(i+1,((RooRealVar*)truthBinVars.at(i))->getVal());
    unfolded->SetBinError(i+1, sqrt(U(i,i)));
    errors->SetBinContent(i+1, sqrt(U(i,i)));
  }

  // Save the results.
  unfolded->Write("unfolded");
  truthTestHist->Write("truth");
  errors->Write("errors");

  output.Close();

  file.Close();


}
