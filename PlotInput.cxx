
void plot_input(TH1D* truth, TH1D* reco, TH1D* recoBkg, TH1D* data, Bool_t logy=false){

  TCanvas c("c","c",900,600);  

  Double_t* recobin = new Double_t[reco->GetNbinsX()+1];

  for (int i = 0; i < reco->GetNbinsX()+1; i++){
    recobin[i] = reco->GetBinLowEdge(i+1);
  }

  TH1F *h1 = new TH1F("h1","h1",reco->GetNbinsX(),recobin);
  TH1F *h2 = new TH1F("h2","h2",reco->GetNbinsX(),recobin);

  for (int i = 0; i < reco->GetNbinsX(); i++){
    
    Double_t binwidth;
    
    if (i < truth->GetNbinsX()){
      binwidth = truth->GetBinLowEdge(i+2) - truth->GetBinLowEdge(i+1);
      truth->SetBinContent(i+1, truth->GetBinContent(i+1)/binwidth);
    }
    binwidth = reco->GetBinLowEdge(i+2) - reco->GetBinLowEdge(i+1);
    h1->SetBinContent(i+1, reco->GetBinContent(i+1)/binwidth);
    h2->SetBinContent(i+1, recoBkg->GetBinContent(i+1)/binwidth);
    data->SetBinContent(i+1, data->GetBinContent(i+1)/binwidth);
  }

  h2->SetFillColor(30);
  h1->SetFillColor(42);
  h2->GetXaxis()->SetTitle("x");
  h1->GetXaxis()->SetTitle("x");
  h1->GetYaxis()->SetTitle("Events");
  h2->GetYaxis()->SetTitle("Events");
			   
  h1->GetYaxis()->SetRangeUser(0.0000001,1.3*data->GetMaximum());
  h2->GetYaxis()->SetRangeUser(0.0000001,1.3*data->GetMaximum());
  data->GetYaxis()->SetRangeUser(0.01,1.3*data->GetMaximum());

  // hs->Draw("stack");
  THStack *hs = new THStack("hs","");
  hs->Add(h2);
  hs->Add(h1);
  hs->SetMinimum(0.001);
  hs->SetMaximum(1.3*data->GetMaximum());
  c.cd(); 
  gPad->SetLogy(logy);
  gStyle->SetOptStat(0);

  data->SetLineColor(kBlack);
  data->SetMarkerStyle(20);
  data->SetMarkerSize(0.8);
  data->GetYaxis()->SetTitle("Events");
  data->GetXaxis()->SetTitle("x");
  data->SetTitle("");

  hs->Draw("");
  data->Draw("PESAME");

  TLegend leg(0.7, 0.7, 0.9, 0.9);
  leg.SetLineWidth(0);
  leg.SetFillStyle(0);
  leg.AddEntry( data, "Measured data", "pe" );
  leg.AddEntry( h1, "Reconstructed Sig MC", "f" );
  leg.AddEntry( h2, "Reconstructed Bkg MC", "f" );
  leg.Draw("SAME");

  c.SaveAs("input_histograms.pdf");
  
}

void plot_resp(TH2D* hist, TMatrixD& resp_vals){
  
  TCanvas c("c","c",900,600);
  
  c.cd();
  
  gStyle->SetOptStat(0);
  gPad->SetTickx(1);
  gPad->SetTicky(1);

  hist->SetTitle("");
  hist->GetXaxis()->SetTitle("x");
  hist->GetYaxis()->SetTitle("y");
  gStyle->SetPalette(kLightTemperature);

  for (int i = 0; i < hist->GetNbinsX(); i++){
    for (int j = 0; j < hist->GetNbinsY(); j++){
      Int_t round_val = (Int_t)(1000*resp_vals(i+1, j+1));
      hist->SetBinContent(i+1,j+1, (Double_t)round_val/1000);
    }
  }

  hist->Draw("COLZ TEXT");

  TLatex l2; 
  l2.SetTextSize(0.04); 
  l2.SetNDC();
  l2.SetTextColor(1);
  l2.SetTextFont(42);

  c.SaveAs("responseMatrix.pdf");

}


void PlotInput(){

  // Get the input file create with Generate.cxx
  std::string inputFile("input_exp.root");
  
  TFile input(inputFile.c_str());
  
  TH1D* truthHist = (TH1D*)input.Get("truthnom");
  TH1D* recoHist = (TH1D*)input.Get("reconom");
  TH1D* recoBkgHist = (TH1D*)input.Get("reco_bkgnom");
  TH2D* responseHist = (TH2D*)input.Get("responsenom");

  TH1D* dataHist = (TH1D*)input.Get("dataHist");

  RooUnfoldSpec spec("unfold","unfold",truthHist,"obs_truth",recoHist,"obs_reco",responseHist,recoBkgHist,dataHist,true,0.0005);

  // Get an unfolding function.
  RooUnfoldFunc* unfoldFunc = (RooUnfoldFunc*)spec.makeFunc(RooUnfolding::kInvert);

  // Instantiate a RooUnfold object with RooFitHist as template type.
  RooUnfoldT<RooUnfolding::RooFitHist,RooUnfolding::RooFitHist>* unfold = const_cast<RooUnfoldT<RooUnfolding::RooFitHist,RooUnfolding::RooFitHist>*>(unfoldFunc->unfolding());

  // Get the response matrix i.e. matrix with bin migration
  // probabilities.
  TMatrixD responseMatrix = unfold->response()->Mresponse();
  
  // Plot the response matrix.
  plot_resp(responseHist, responseMatrix);
  
  // Plot the input distribution in one canvas.
  plot_input(truthHist, recoHist, recoBkgHist, dataHist, false);
  
}
